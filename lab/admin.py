from django.contrib import admin
from lab.models import Lock
from simple_history.admin import SimpleHistoryAdmin

# from guardian.admin import GuardedModelAdmin
# Register your models here.

# class LockAdmin(GuardedModelAdmin):
# 	pass

admin.site.register(Lock,SimpleHistoryAdmin)
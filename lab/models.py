from django.contrib.auth.models import User
from django.db import models
from simple_history.models import HistoricalRecords

# Create your models here.


class Lock(models.Model):
	name = models.CharField(max_length=50)
	location = models.CharField(max_length=100)
	desc = models.TextField()
	state = models.BooleanField(default=False)
	users = models.ManyToManyField(User)
	history = HistoricalRecords()

	def __unicode__(self):
		return self.name

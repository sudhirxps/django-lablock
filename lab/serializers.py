from django.contrib.auth.models import User, Group
from rest_framework	 import serializers
from lab.models import Lock

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups','first_name','password')

    def create(self, validated_data):
        user = User.objects.create(
            username=validated_data['username'],
            email=validated_data['email'],
            first_name=validated_data['first_name'],
        )

        user.set_password(validated_data['password'])
        user.save()

        return user


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')

class LockSerializer(serializers.HyperlinkedModelSerializer):
	
	class Meta:
		model = Lock
		fields = ('url','name','state','users')        

	def update(self, instance, validated_data):
		labEmpty = True

		instance.state = validated_data.get("state",instance.state) 

		if labEmpty and validated_data.get("state",instance.state):
			# Turn off the lights			
			pass
		
		instance.save()

		return instance

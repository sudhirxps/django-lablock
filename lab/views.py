from django.contrib.auth.models import User, Group
from rest_framework import viewsets,filters
from lab.serializers import UserSerializer, GroupSerializer, LockSerializer
from lab.models import Lock
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    filter_fields = ('email',)
    filter_backends = (filters.DjangoFilterBackend,)
    
class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    

class LockViewSet(viewsets.ModelViewSet):

    queryset = Lock.objects.all()
    serializer_class = LockSerializer
    
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('users',)
    authentication_classes = (TokenAuthentication,SessionAuthentication,BasicAuthentication)
    permission_classes = (IsAuthenticated,)

